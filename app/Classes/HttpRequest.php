<?php

namespace App\Classes;

use Illuminate\Support\Facades\Http;

class HttpRequest
{
    public static function get($uri, $data=[])
    {
        return self::request('get', $uri, $data);
    }

    public static function post($uri, $data=[])
    {
        return self::request('post', $uri, $data);
    }

    public static function delete($uri, $data=[])
    {
        return self::request('delete', $uri);
    }

    public static function request($method, $uri, $data=[])
    {
        try {
            $token = Storage::get('token');
            if($token)
            {
                $result = Http::withHeaders(['Accept' => 'application/json'])->withToken($token)->{$method}(env('API_URL').$uri, $data);
                if($result->status() == 401)
                {
                    Storage::remove('token');
                    abort(401);
                }
                return $result;
            }

            return Http::withHeaders(['Accept' => 'application/json'])->{$method}(env('API_URL').$uri, $data);
        }
        catch(\Exception $e) {
            dd($e);
            return abort('500', $e->getMessage());
        }
    }
}
