<?php

namespace App\Classes;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;

class Storage
{
    public static function set($key, $val)
    {
        // return Cache::put($key, $val);
        return setcookie($key, $val);
    }

    public static function get($key)
    {
        // return Cache::get($key);
        return @$_COOKIE[$key];
    }

    public static function has($key)
    {
        if(isset($_COOKIE[$key]))
            return true;
        return false;
    }

    public static function remove($key)
    {
        if (isset($_COOKIE[$key])) {
            unset($_COOKIE[$key]); 
            setcookie($key, null, -1, '/'); 
            return true;
        } else {
            return false;
        }
    }
}
