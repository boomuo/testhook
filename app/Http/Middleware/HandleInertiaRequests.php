<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Inertia\Middleware;
use Tightenco\Ziggy\Ziggy;
use App\Classes\Storage;
use App\Classes\HttpRequest;
use Illuminate\Support\Facades\App;

class HandleInertiaRequests extends Middleware
{
    /**
     * The root template that is loaded on the first page visit.
     *
     * @var string
     */
    protected $rootView = 'app';

    /**
     * Determine the current asset version.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function version(Request $request)
    {
        return parent::version($request);
    }

    /**
     * Define the props that are shared by default.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function share(Request $request)
    {
        $result = HttpRequest::get('/data')->json();

        $result['config']['background'] = '#54b8ff';
        $result['config']['logo_url'] = 'https://images.unsplash.com/photo-1554629947-334ff61d85dc?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1024&h=1280&q=80';

        $request->session()->put('user', @$result['user']);
        $request->session()->put('config', @$result['config']);

        return array_merge(parent::share($request), [
            'locale' => app()->getLocale(),
            'language' => function () {
                return translations(
                    base_path('lang/'. app()->getLocale() .'.json')
                );
            },
            'config' => $request->session()->get('config'),
            'auth' => [
                // 'user' => $request->user(),
                'user' => $request->session()->get('user'),
                // 'user' => json_decode(Storage::get('user')),
            ],
            'ziggy' => function () use ($request) {
                return array_merge((new Ziggy)->toArray(), [
                    'location' => $request->url(),
                    'query'=>$request->query()
                ]);
            },
            'flash' => [
                'message' => fn () => $request->session()->get('message'),
                'success' => fn () => $request->session()->get('success'),
                'warning' => fn () => $request->session()->get('warning'),
                'error' => fn () => $request->session()->get('error'),
            ],
        ]);
    }
}
