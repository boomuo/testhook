<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Classes\HttpRequest;
use App\Classes\Storage;
use Inertia\Inertia;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index(Request $request)
    {
        $result = HttpRequest::get('/product', $request->all())->json();

        return Inertia::render('ProductList', [
            'products' => @$result['data'],
            'filters' => @$result['filters'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Inertia\Response
     */
    public function show($id=false)
    {
        $product = HttpRequest::get('/product/'.$id)->json();

        if(!$product)
            return abort(404);

        $paymentSystems = HttpRequest::get('/payment-system-list')->json();

        return Inertia::render('ProductItem', [
            'product' => $product['data'],
            'paymentSystems' => $paymentSystems['data'],
            'url' => url('')
        ]);
    }

    // public function getItem(Request $request)
    // {
    //     dd($request->all());
    //     $product = HttpRequest::get('/product/get-item', $request->all())->json();
    //     if(!$product)
    //         return abort(404);
    //     $paymentSystems = HttpRequest::get('/payment-system-list')->json();
    //     return Inertia::render('ProductItem', [
    //         'product' => $product['data'],
    //         'paymentSystems' => $paymentSystems['data'],
    //         'url' => url('')
    //     ]);
    // }
    
    // public function getCity(Request $request)
    // {
    //     $products = HttpRequest::get('/items')->json();

    //     return Inertia::render('CityList', [
    //         'cities' => @$products['data'],
    //     ]);
    // }

    // public function getSort(Request $request, $city_id)
    // {
    //     $products = HttpRequest::get('/items/'.$city_id)->json();

    //     return Inertia::render('SortList', [
    //         'city_id' => $city_id,
    //         'sorts' => @$products['data'],
    //     ]);
    // }

    // public function getArea(Request $request, $city_id, $sort_id)
    // {
    //     $products = HttpRequest::get('/items/'.$city_id.'/'.$sort_id)->json();

    //     return Inertia::render('AreaList', [
    //         'areas' => @$products['data'],
    //     ]);
    // }
}
