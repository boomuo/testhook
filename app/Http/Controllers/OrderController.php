<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\HttpRequest;
use Inertia\Inertia;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $result = HttpRequest::get('/order')->json();

        return Inertia::render('OrderList', [
            'orders' => $result['data'],
        ]);
    }

    public function store(Request $request)
    {
        $result = HttpRequest::post('/order', $request->all())->json();

        // if OK return to payment method
        if($result && isset($result['id']))
            return redirect()->route('order.show', $result['id'])->with('success', 'Товар забронирован');

        return redirect()->back()->with('error', 'Ошибка при создании заказа');
    }

    public function show($id)
    {
        $result = HttpRequest::get('/order/'.$id)->json();
        $config = session('config');

        if(!$result)
            return abort(404);

        if($result['status'] == 'payment_wait')
        {
            // выбор системы оплаты
            $template = 'PaymentSystems/'.$result['payment_request']['payment_system_key'];
            $text = @$config['payment_'.$result['payment_request']['payment_system_key']];
        }

        if($result['status'] == 'payment_wait_check')
        {
            // ожидание оплаты
            $template = 'WaitPaymentCheck';
            $text = @$config['payment_wait_check'];
        }

        if($result['status'] == 'payment_fail')
        {
            // оплата не прошла
            $template = 'PaymentFail';
            $text = @$config['payment_fail'];
        }

        // оплата прошла успешно
        if($result['status'] == 'payment_complete')
        {
            if($result['type'] == 'balance')
            {
                $template = 'PaymentBalanceComplete';
                $text = @$config['payment_balance_complete'];
            }
            else 
            {
                $template = 'PaymentComplete';
                $text = @$config['payment_complete'];
            }
    
        }


        return Inertia::render($template, [
            'order' => $result,
            'text' => $this->parser($result, $text),
            'intervalPageReload' => @$config['interval_page_reload'] ?? 30,
        ]);
    }

    public function destroy(Request $request, $order_id)
    {
        $result = HttpRequest::delete('/order/'.$order_id)->json();
        return redirect()->route('shop.index')->with('warning', 'Заказ удален');
    }

    public function payment(Request $request)
    {
        $response = HttpRequest::post('/payment', $request->all());
        $result = $response->json();

        // dd($result);

        if(!isset($response['status']))
        {
            return redirect()->route('shop.index')->with('error', 'Ошибка добавления платежа');
        }

        if(isset($result['error']))
        {
            return redirect()->route('shop.index')->with('error', $result['error']);
        }


        if(@$response['status'] == 102 && !isset($result['error']))
        {
            return redirect()->back()->with('success', 'Платеж добавлен в обработку');
        }

        if(@$response['status'] == 200 && !isset($result['error']))
        {
            return redirect()->route('shop.show', $result['order']['id'])->with('success', 'Платеж одобрен');
        }
        

        return redirect()->route('shop.index')->with('error', 'Ошибка');
    }

    public function parser($order, $text)
    {
        $template_array = [];

        @$template_array['%ORDER_ID%'] = $order['id'];
        @$template_array['%ORDER_STATUS%'] = $order['statusTitle'];
        @$template_array['%CLIENT_ID%'] = $order['client_id'];
        @$template_array['%PRICE%'] = "{$order['payment_request']['price']} {$order['payment_request']['currency']}";
        @$template_array['%PAYMENT_SYSTEM%'] = $order['payment_request']['payment_system_key'];
        @$template_array['%WALLET%'] = ($order['payment_request']['data']['wallet']) ?? '';
        @$template_array['%URL_PAYMENT%'] = ($order['payment_request']['data']['url']) ?? '';
        @$template_array['%TIME_LEFT_PAYMENT%'] = date('H:i d-m-Y', strtotime($order['data']['time_left_payment']));

        // @$template_array['%CITY%'] = $order['item']['city']['name'];
        // @$template_array['%AREA%'] = $order->item->area->name;
        // @$template_array['%CATEGORY%'] = $order->item->sort->category->name;
        // @$template_array['%CATEGORY_DESCRIPTION%'] = $order->item->sort->category->description;
        // @$template_array['%SORT%'] = $order->item->sort->name;
        // @$template_array['%SORT_DESCRIPTION%'] = $order->item->sort->description;
        // @$template_array['%SORT_IMG%'] = $order->item->sort->img;
        // @$template_array['%PACKING%'] = "{$order->item->packing->packing} {$order->item->packing->unit}";
        // @$template_array['%ITEM_PRICE%'] = "{$order->item->packing->price} {$order->item->packing->currency}";
        // @$template_array['%COORDINATES%'] = "http://maps.google.com/?q={$order->item->coordinates}";
        // @$template_array['%DESCRIPTION%'] = $order->item->description;

        foreach ($template_array as $key => $value)
            $text = str_replace($key, $value, $text);

        return $text;
    }
}
