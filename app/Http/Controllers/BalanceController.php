<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\HttpRequest;
use Inertia\Inertia;


class BalanceController extends Controller
{
    public function index(Request $request)
    {
        $paymentSystems = HttpRequest::get('/payment-system-list-balance')->json();
        return Inertia::render('Balance', [
            'paymentSystems' => $paymentSystems['data'],
            'url' => url('')
        ]);
    }

    public function store(Request $request)
    {
        $result = HttpRequest::post('/balance/payment', $request->all())->json();

        // if OK return to payment method
        if($result && isset($result['id']))
            return redirect()->route('order.show', $result['id']);
        return false;
    }
}
