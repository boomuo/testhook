<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Http;
use App\Classes\Storage;
use App\Classes\HttpRequest;
use Inertia\Inertia;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Auth/Register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'login' => 'required|string|min:3|max:25',
            'password' => ['required', 'confirmed'],
            // 'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $result = HttpRequest::post('/user/register', $request->all())->json();

        if(isset($result['token']))
        {
            Storage::set('token', $result['token']);
            Storage::set('user', json_encode($result['user']));

            // event(new Registered($user));

            // Auth::login($user);

            return redirect()->intended('/');
        }

        throw ValidationException::loginwithMessages([
            'login' => trans('auth.failed'),
        ]);
    }
}
