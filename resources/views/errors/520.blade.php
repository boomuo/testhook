@extends('errors::minimal')

@section('title', $exception->getMessage())
@section('code', '520')
@section('message', $exception->getMessage())
