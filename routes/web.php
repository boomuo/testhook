<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use Illuminate\Support\Facades\Cookie;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {

    // return env('API_URL');
    // dd(config('api.url'));
    dd($_COOKIE);
    dd(env('API_URL'));


    // session('test', 'qqqqq');
    // $value = session('test');

    // Session::push('test', 'value');
    // $value = Session::get('test');

    // $_COOKIE['test'] = 'qweqwe';
    // setcookie('test', 'sadsd');
    // $value = @$_COOKIE['test'];

    // Cookie('test', 'qweqwe');
    // $value = Cookie::get('test');

    // $value = cookie('test')->getValue();

    // dd($value);
});

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

// Route::get('/dashboard', function () {
//     // return Inertia::render('Dashboard');
// })->middleware(['auth:sanctum', 'verified'])->name('dashboard');

Route::get('login', [\App\Http\Controllers\Auth\AuthenticatedSessionController::class, 'create'])->name('login');
Route::post('login', [\App\Http\Controllers\Auth\AuthenticatedSessionController::class, 'store'])->name('login.store');
Route::get('register', [\App\Http\Controllers\Auth\RegisteredUserController::class, 'create'])->name('register');
Route::post('register', [\App\Http\Controllers\Auth\RegisteredUserController::class, 'store'])->name('register.store');


Route::get('/', function () {
    return redirect()->route('shop.index');
});

Route::group(['middleware' => ['auth.user']], function(){
    Route::get('/shop', [\App\Http\Controllers\ShopController::class, 'index'])->name('shop.index');
    Route::get('/item/{id}', [\App\Http\Controllers\ShopController::class, 'show'])->name('item.show');
    // Route::get('/shop/get-item', [\App\Http\Controllers\ShopController::class, 'getItem'])->name('shop.show');
    // Route::get('/shop', [\App\Http\Controllers\ShopController::class, 'getCity'])->name('shop.index');
    // Route::get('/shop/{city_id}', [\App\Http\Controllers\ShopController::class, 'getSort'])->name('shop.sort');
    // Route::get('/shop/{city_id}/{sort_id}', [\App\Http\Controllers\ShopController::class, 'getArea'])->name('shop.area');



    // Route::resource('/shop', \App\Http\Controllers\ShopController::class)->only(['index', 'show']);
    Route::resource('order', \App\Http\Controllers\OrderController::class);
    Route::post('payment', [\App\Http\Controllers\OrderController::class, 'payment'])->name('order.payment');
    Route::get('balance', [\App\Http\Controllers\BalanceController::class, 'index'])->name('balance.index');
    Route::post('balance', [\App\Http\Controllers\BalanceController::class, 'store'])->name('balance.store');
    Route::post('logout', [\App\Http\Controllers\Auth\AuthenticatedSessionController::class, 'destroy'])->name('logout');
    
    // Route::post('order', [\App\Http\Controllers\OrderController::class, 'store'])->name('order.store');
    // Route::post('order-cancel', [\App\Http\Controllers\OrderController::class, 'destroy'])->name('order.destroy');
    // Route::get('order/{id}', [\App\Http\Controllers\OrderController::class, 'show'])->name('order.show');
    // Route::delete('order/destroy', [\App\Http\Controllers\OrderController::class, 'destroy'])->name('order.destroy');
});
